let pokemonTrainer = {
    trainerName : "Ash Ketchum",
    trainerAge : 10,
    pokemon : ["Pikachu","Charmander","Squirtle","Bulbasaur"],
    friends : [
        kanto = ["Misty","Brock"],
        koen = ["May","Max"]
    ],
}
console.log(pokemonTrainer);
console.log(pokemonTrainer.trainerName);

pokemonTrainer.talk = function(pokemon){
    console.log(pokemon + " I choose you!");
}
pokemonTrainer.talk(pokemonTrainer.pokemon[3]);


function pokemon(name,level){
    this.pokemonName = name;
    this.pokemonLevel = level;
    this.pokemonHealth = 2 * level;
    this.pokemonAttack = level;
    this.tackle = function(targetPokemon){
        console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
        console.log(targetPokemon.pokemonName + " health is now reduced to " + (targetPokemon.pokemonHealth - this.pokemonAttack));
        targetPokemon.pokemonHealth = targetPokemon.pokemonHealth - this.pokemonAttack;
        if(targetPokemon.pokemonHealth <= 0){
            targetPokemon.faint();
        }
    }
    this.faint = function(){
        console.log(this.pokemonName + " has fainted");
    }
    
}

let Pikachu = new pokemon(pokemonTrainer.pokemon[0],24);
console.log(Pikachu);
let Charmander = new pokemon(pokemonTrainer.pokemon[1],20);
console.log(Charmander);
let Squirtle = new pokemon(pokemonTrainer.pokemon[2],32);
console.log(Squirtle);
let Bulbasaur = new pokemon(pokemonTrainer.pokemon[3],22);
console.log(Bulbasaur);
let Blastoise = new pokemon("Blastoise",12);
console.log(Blastoise);
Pikachu.tackle(Blastoise);
    
